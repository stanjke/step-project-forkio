const toggleBtn = document.querySelector(".toggle-button");
const navWrap = document.querySelector(".nav__wrap");
const navLinks = document.querySelectorAll(".nav__link");
const headerWrap = document.querySelector(".header-wrap");

let pageNumber = 0;
let screenWidth = null;
// let scrollWidth = null;

let getIndex = (clickedElem) => {
  if (clickedElem.tagName.toLowerCase() === "a") {
    let listItem = clickedElem.parentNode;
    let list = listItem.parentNode;
    let index = [...list.children].indexOf(listItem);
    return index;
  }
};

document.addEventListener("click", (e) => {
  screenWidth = document.documentElement.clientWidth;
  if (screenWidth <= 480) {
    if (e.target.closest(".toggle-button") !== null) {
      toggleBtn.classList.toggle("toggle-button--active");
      navWrap.classList.toggle("nav__wrap--active");
    } else if (e.target.closest(".nav__wrap")) {
      [...navLinks].forEach((e) => e.classList.remove("nav__link--active"));
      e.target.classList.add("nav__link--active");
      pageNumber = getIndex(e.target);
    } else {
      navWrap.classList.remove("nav__wrap--active");
      toggleBtn.classList.remove("toggle-button--active");
    }
  }
});

let checkResolution = () => {
  screenWidth = document.documentElement.clientWidth;
  scrollWidth = window.innerWidth - screenWidth;

  if (screenWidth >= 480 - scrollWidth) {
    [...navLinks].forEach((e) => e.classList.remove("nav__link--active"));
    navWrap.classList.remove("nav__wrap--active");
    toggleBtn.classList.remove("toggle-button--active");
  } else if (screenWidth < 480) {
    [...navLinks].forEach((e) => e.classList.remove("nav__link--active"));
    navLinks[pageNumber].classList.add("nav__link--active");
  }
  if (screenWidth >= 1080) {
    headerWrap.classList.add("container");
  } else {
    headerWrap.classList.remove("container");
  }
};

window.addEventListener("load", checkResolution);
window.addEventListener("resize", checkResolution);
